const dummy = {
    countryName:'United States',
    countryAlpha2Code:'US',
    subdivisionName:'Washington',
    subdivisionCode:'WA'
};

/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default dummy;