import Iso3166Sdk from '../../src/index';

describe('Index module', () => {

    describe('default export', () => {
        it('should be Iso3166Sdk constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new Iso3166Sdk();

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(Iso3166Sdk));

        });
    });

    describe('instance of default export', () => {

        describe('listCountries method', () => {
            it('should return more than 1 result', (done) => {

                /*
                 arrange
                 */

                const objectUnderTest =
                    new Iso3166Sdk();

                /*
                 act
                 */
                const countriesPromise = objectUnderTest.listCountries();

                /*
                 assert
                 */

                countriesPromise
                    .then((countries) => {
                        expect(countries.length).toBeGreaterThan(1);
                        done();
                    })
                    .catch((error)=> {
                        fail(error);
                        done();
                    });

            });
        });

        describe('listSubdivisionsWithAlpha2CountryCode method', () => {
            it('should return 50 results for United States alpha 2 country code', (done) => {

                /*
                 arrange
                 */

                const objectUnderTest =
                    new Iso3166Sdk();

                const unitedStatesAlpha2CountryCode = 'US';

                /*
                 act
                 */
                const subdivisionsPromise =
                    objectUnderTest.listSubdivisionsWithAlpha2CountryCode(
                        unitedStatesAlpha2CountryCode
                    );

                /*
                 assert
                 */

                subdivisionsPromise
                    .then((subdivisions) => {
                        expect(subdivisions.length).toEqual(57);
                        done();
                    })
                    .catch((error)=> {
                        fail(error);
                        done();
                    });

            })
        });

    }, 30000);
});